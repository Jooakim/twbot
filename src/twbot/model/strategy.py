from abc import ABCMeta, abstractmethod
from collections import defaultdict


class Strategy(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.update_order = [
            ("wood", 1),
            ("stone", 1),
            ("iron", 1),
            ("wood", 2),
            ("stone", 2),
            ("iron", 2),
            ("main", 3),
            ("barracks", 1),
            ("farm", 1),
            ("main", 5),
            ("smith", 2),
            ("wood", 3),
            ("stone", 3),
            ("iron", 3),
            ("barracks", 5),
            ("main", 8),
            ("smith", 3),
            ("main", 9),
            ("smith", 4),
            ("main", 10),
            ("smith", 5),
            ("stable", 3),
            ("market", 1),
            ("farm", 5),
            ("wood", 4),
            ("stone", 4),
            ("iron", 4),
            ("wood", 5),
            ("stone", 5),
            ("iron", 5),
            ("wood", 6),
            ("stone", 6),
            ("iron", 6),
            ("farm", 10),
            ("wood", 7),
            ("stone", 7),
            ("iron", 7),
            ("wood", 8),
            ("stone", 8),
            ("iron", 8),
            ("storage", 7),
            ("wood", 9),
            ("stone", 9),
            ("iron", 9),
            ("wood", 10),
            ("stone", 10),
            ("iron", 12),
            ("farm", 14),
            ("smith", 10),
            ("garage", 2),
            ("farm", 15),
            ("barracks", 10),
            ("smith", 12),
            ("farm", 18),
            ("barracks", 15),
            ("stable", 10),
            ("main", 18),
            ("wood", 16),
            ("iron", 16),
            ("stone", 16),
            ("wood", 17),
            ("iron", 17),
            ("stone", 17),
            ("wood", 18),
            ("iron", 18),
            ("stone", 18),
            ("farm", 26),
            ("stable", 15),
            ("main", 20),
            ("farm", 27),
            ("smith", 20),
            ("farm", 28),
            ("barracks", 20),
            ("storage", 24),
            ("farm", 29),
            ("snob", 1),
            ("market", 10),
            ("stable", 20),
            ("barracks", 25),
            ("wall", 20),
        ]

    @abstractmethod
    def get_unit_to_train(self, current_units):
        pass
    @abstractmethod
    def get_stable_unit(self, current_units):
        pass

    def _get_unit_to_train(self, current_units, targets):
        unit_diffs = dict()
        for unit, target in targets.items():
            unit_diffs[unit] = target - current_units[unit]

        unit=sorted(unit_diffs.items(), key=lambda x:x[1], reverse=True)[0][0]
        return unit


    def get_building_to_update(self, buildings):
        for item in self.update_order:
            if buildings[item[0]] < item[1]:
                return (item[0], buildings[item[0]])
        return None


class PassiveStrategy(Strategy):
    def get_building_to_update(self, *args, **kwargs):
        return None
    def get_stable_unit(self):
        return None
    def get_unit_to_train(self):
        return None
