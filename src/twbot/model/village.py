from collections import defaultdict
from typing import Tuple

from twbot.model.strategy import Strategy


class Village:
    def __init__(self, village_id: int, choords: Tuple[int, int], strategy: Strategy):
        self.village_id = village_id
        self.choords = choords
        self.strategy = strategy

    def get_building_to_update(self, buildings):
        building = self.strategy.get_building_to_update(buildings)
        print(f"next building to update: {building}")
        return building

    def get_stable_unit(self, current_units: defaultdict):
        return self.strategy.get_stable_unit(current_units)

    def get_barrack_unit(self, current_units: defaultdict):
        return self.strategy.get_unit_to_train(current_units)

    def get_village_position(self):
        return self.choords
