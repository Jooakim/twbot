from dataclasses import dataclass, field
from datetime import datetime
import functools
import time
from typing import Dict, List, Optional

from selenium.webdriver import Firefox
from selenium.webdriver.remote.webelement import WebElement
from sqlalchemy import desc
from sqlalchemy.orm.session import Session
from sqlalchemy.sql.schema import Column, MetaData, Table
from sqlalchemy.sql.sqltypes import Integer, String, TIMESTAMP, JSON
from sqlalchemy.orm import mapper
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

@dataclass
class Report:
    id: int = None
    choords: str = field(default_factory=str)
    haul: int = field(default_factory=int)
    capacity: int = field(default_factory=int)
    time: datetime = field(default_factory=datetime.now)
    attack_force: Dict[str, int] = field(default_factory=dict)
    losses: Dict[str, int] = field(default_factory=dict)
    scout_report: Dict[str, int] = field(default_factory=dict)


class ReportController:
    ATTACK_NAME = "FARM"

    metadata = MetaData()
    report_table = Table(
        "report",
        metadata,
        Column("id", Integer, primary_key=True, autoincrement=True),
        Column("choords", String(255)),
        Column("haul", Integer),
        Column("capacity", Integer),
        Column("time", TIMESTAMP),
        Column("attack_force", JSON),
        Column("losses", JSON),
        Column("scout_report", JSON),
    )
    mapper(Report, report_table)

    def __init__(self, driver: Firefox, base_url, session: Session) -> None:
        self.driver = driver
        self.base_url = base_url
        self.session = session
        self.metadata.create_all(session.get_bind())

    @functools.lru_cache(maxsize=1)
    def get_last_report(self, choords) -> Optional[Report]:
        reports = self.get_last_x_reports(choords, 1)
        return reports[-1] if reports else None

    def get_last_x_reports(self, choords, n_reports) -> List[Report]:
        reports = (
            self.session.query(Report)
            .filter(self.report_table.c.choords == choords)
            .order_by(desc(self.report_table.c.time))
            .limit(n_reports)
        )
        return [x for x in reports]

    def read_reports(self):
        result = self.session.execute("SELECT Count(*) FROM report")
        n_rows = next(iter(result))[0]
        if n_rows == 0:
            reports = self._read_all_reports()
        else:
            reports = self._read_new_reports()
        self.session.expunge_all()
        self.session.add_all(reports)
        self.session.commit()

    def _read_new_reports(self):
        self._visit_report()
        reports = []
        has_premium = False
        if has_premium:
            # apply filter -- need premium
            filter_input = self.driver.find_element_by_id('filter_subject')
            filter_input.clear()
            filter_input.send_keys(self.ATTACK_NAME)
            self.driver.find_element_by_class_name('report_filter').find_element_by_class_name('btn').click()

            while True:
                num_reports = len(self.driver.find_elements_by_class_name("report-link"))
                if num_reports == 0:
                    return reports

                self.driver.find_elements_by_class_name("report-link")[0].click()
                for _ in range(num_reports):
                    time.sleep(0.1)
                    reports.append(self._read_report())
                    self.driver.find_element_by_id("report-prev").click()
                self._visit_report()
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                self.driver.find_element_by_id('select_all').click()
                self.driver.execute_script("window.scrollTo(document.body.scrollHeight,0);")
                unreads = self.driver.find_elements_by_class_name('unread')
                for unread in unreads:
                    unread.find_element_by_tag_name('input').click()

                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                select_element = self.driver.find_elements_by_tag_name('select')[-1]
                select = Select(select_element)
                if 'FARM' in select_element.text:
                    select.select_by_visible_text('FARM')
                else:
                    select.select_by_visible_text('Archive')
                next(btn for btn in self.driver.find_elements_by_tag_name('input') if btn.get_attribute('value') == 'Move').click()
                #select_element.parent.find_element_by_class_name('btn').click()
            #read_reports = [report for report in reports if report.

            # while True:
            #     # FIXME this is slow af
            #     # Would be better to read all first, then bulk-move
            #     report_links = self.driver.find_elements_by_class_name("report-link")
            #     if len(report_links) == 0:
            #         break
            #     report_links[0].click()
            #     next(btn for btn in self.driver.find_elements_by_tag_name('a') if btn.text == 'Move').click()
            #     select_element = self.driver.find_element_by_tag_name('select')
            #     reports.append(self._read_report())
            #     select = Select(select_element)
            #     if 'FARM' in select_element.text:
            #         select.select_by_visible_text('FARM')
            #     else:
            #         select.select_by_visible_text('Archive')
            #     select_element.parent.find_element_by_class_name('btn').click()
        else:
            num_reports = len(self.driver.find_elements_by_class_name("unread"))
            self.driver.find_elements_by_class_name("report-link")[0].click()
            for _ in range(num_reports):
                time.sleep(0.1)
                reports.append(self._read_report())
                self.driver.find_element_by_id("report-prev").click()
        return reports

    def _read_all_reports(self):
        self._visit_report()
        self.driver.find_elements_by_class_name("report-link")[0].click()
        reports = []
        while True:
            time.sleep(0.1)
            reports.append(self._read_report())
            try:
                self.driver.find_element_by_id("report-prev").click()
            except:
                print("no more reports")
                break
        return reports

    def _read_report(self):

        choords = (
            self.driver.find_elements_by_class_name("village_anchor")[1]
            .find_element_by_tag_name("a")
            .text.split("(")[1][:7]
        )

        try:
            attack_info = self.driver.find_element_by_id("attack_results").text.split(
                "\n"
            )
            haul_info = attack_info[0].replace(".", "").split()[-1].split("/")
        except:
            haul_info = [0, 0]
        haul = int(haul_info[0])
        capacity = int(haul_info[1])

        tables = self.driver.find_elements_by_xpath("//table[@class='vis']")
        timestamp = tables[2].find_elements_by_tag_name("td")[1].text
        report_time = datetime.strptime(timestamp[:-4], "%B %d, %Y %H:%M:%S")
        attack_tables = self.driver.find_element_by_id(
            "attack_info_att_units"
        ).find_elements_by_tag_name("tr")
        attack_force = self._fetch_table_data(attack_tables[1])
        losses = self._fetch_table_data(attack_tables[2])
        scout_report = {}
        try:
            scout_report.update(
                {
                    " ".join(x.split()[:-1]): int(x.split()[-1])
                    for x in self.driver.find_element_by_id(
                        "attack_spy_buildings_left"
                    ).text.strip().split("\n")[1:]
                }
            )
            scout_report.update(
                {
                    " ".join(x.split()[:-1]): int(x.split()[-1])
                    for x in self.driver.find_element_by_id(
                        "attack_spy_buildings_right"
                    ).text.strip().split("\n")[1:]
                }
            )
            ram_damage_info = attack_info[1]
            if "Damage by rams" in ram_damage_info:
                scout_report["Wall"] = int(ram_damage_info.split()[-1])

        except:
            pass

        return Report(
            choords=choords,
            haul=haul,
            capacity=capacity,
            time=report_time,
            attack_force=attack_force,
            losses=losses,
            scout_report=scout_report,
        )

    def _fetch_table_data(self, table):
        units = [
            "spear",
            "sword",
            "axe",
            "archer",
            "spy",
            "light",
            "marcher",
            "heavy",
            "ram",
            "catapult",
            "snob",
        ]
        return {k: v for k, v in zip(units, map(int, table.text.split()[1:]))}

    def _visit_report(self):
        report_url = "&screen=report&mode=attack"
        self.driver.get(self.base_url + report_url)
        time.sleep(2)

    def get_all_reports(self):
        return self.session.query(Report).all()
