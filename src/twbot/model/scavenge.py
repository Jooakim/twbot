from collections import defaultdict
import math


class SimpleScavenger:
    def __init__(self, units: dict, scavenger_divs):
        self.max_units = units
        count = 0
        for i in range(4):
            try:
                scavenger_divs[i].find_element_by_class_name("locked-view")
            except:
                count = count + 1
        # self.max_units['spear'] = 270
        # self.max_units['sword'] = 7
        self.max_units["axe"] = 0
        # if 'axe' in units:
        #     self.max_units['axe']  = self.max_units['axe'] - 300
        # self.max_units['spear']  = self.max_units['spear'] - 273
        self.unlocked_levels = count
        print(f"{self.unlocked_levels} unlocked scavenge levels")
        self.carrying_capacity = {"spear": 25, "sword": 15, "axe": 10}
        self.level_ratio = {1: 0.1, 2: 0.25, 3: 0.5, 4: 0.75}

        total_cap = 0
        for unit in ["spear", "sword", "axe"]:
            total_cap = total_cap + (
                self.max_units[unit] * self.carrying_capacity[unit]
            )
        self.total_cap = total_cap

    def generate_optimal_scavenging_spread(self):
        print(self.max_units)

        spread = [1 / self.unlocked_levels] * self.unlocked_levels
        if self.unlocked_levels > 1:
            permutations = [0] * self.unlocked_levels
            highest_haul = self._resources_per_hour(spread)
            old_haul = -1
            while True:
                for j in reversed(range(self.unlocked_levels)):
                    spread2 = spread[:]
                    spread2[j] = spread2[j] / 2
                    spread2[j - 1] = spread2[j - 1] + spread2[j]
                    haul2 = self._resources_per_hour(spread2)

                    spread3 = spread[:]
                    spread3[j - 1] = spread3[j - 1] / 2
                    spread3[j] = spread3[j] + spread3[j - 1]
                    haul3 = self._resources_per_hour(spread3)

                    if haul2 > highest_haul:
                        highest_haul = haul2
                        spread = spread2
                    if haul3 > highest_haul:
                        highest_haul = haul3
                        spread = spread3

                if old_haul == highest_haul:
                    break

                old_haul = highest_haul

        optimal_units = self._translate_carrying_capacity_to_units(spread)
        print(highest_haul, optimal_units)

        return optimal_units

    def _resources_per_hour(self, spread: list):
        game_speed = 1.5
        df = game_speed ** (-0.55)
        haul = 0
        for i in range(self.unlocked_levels):
            modified_cap = self.total_cap * spread[i]
            scav_brings_in = modified_cap * self.level_ratio[i + 1]
            duration_in_sec = (
                math.pow(
                    math.pow(modified_cap, 2)
                    * 100
                    * math.pow(self.level_ratio[i + 1], 2),
                    0.45,
                )
                + 1800
            ) * df
            a = scav_brings_in / duration_in_sec
            haul = haul + a

        return haul * 3600 # per hour

    def _translate_carrying_capacity_to_units(self, spread: list):
        print(spread)
        returnValues = {}
        goals = [0] * len(spread)
        for i in range(len(spread)):
            goals[i] = int(self.total_cap * spread[i])
            returnValues[i] = defaultdict(int)

        spears_capacity = self.max_units["spear"] * self.carrying_capacity["spear"]
        sword_capacity = self.max_units["sword"] * self.carrying_capacity["sword"]
        axe_capacity = self.max_units["axe"] * self.carrying_capacity["axe"]
        for i, goal_capacity in reversed(list(enumerate(goals))):
            if spears_capacity > goal_capacity:
                nr_spears = int(goal_capacity / self.carrying_capacity["spear"])
                spears_capacity = spears_capacity - goal_capacity
                returnValues[i]["spear"] = nr_spears
            else:
                rest = goal_capacity - spears_capacity
                nr_spears = int(spears_capacity / self.carrying_capacity["spear"])
                spears_capacity = 0
                returnValues[i]["spear"] = nr_spears

                if sword_capacity > rest:
                    nr_swords = int(rest / self.carrying_capacity["sword"])
                    sword_capacity = sword_capacity - rest
                    returnValues[i]["sword"] = nr_swords

                else:
                    rest = rest - sword_capacity
                    nr_swords = int(sword_capacity / self.carrying_capacity["sword"])
                    sword_capacity = 0
                    returnValues[i]["sword"] = nr_swords

                    if axe_capacity > rest:
                        nr_axes = int(rest / self.carrying_capacity["axe"])
                        axe_capacity = axe_capacity - rest
                        returnValues[i]["axe"] = nr_axes

                    else:
                        nr_axes = int(axe_capacity / self.carrying_capacity["axe"])
                        axe_capacity = 0
                        returnValues[i]["axe"] = nr_axes
        return returnValues
