from datetime import datetime
import math
import os
from ..point_table import CUMULATIVE_POINT_TABLE
from typing import Dict, Iterable, Optional
from twbot.model.db.barbs import BarbController, BarbStatus, Barbarian

from twbot.model.db.report import ReportController


class SimpleFarmer:
    ATTACK_INTERVAL_IN_SEC = 20 * 60
    WOLRD_SPEED = 1.5

    LC_SPEED_SEC_PER_FIELD = 10 * 60
    TARGET_EFFICIENCY = 0.5
    LC_CARRY_CAPACITY = 80
    MAXIMUM_LC_AT_A_TIME = 3
    UNKNOWN_POINTS_TO_TRIGGER_NEW_SPY_ATTACK = 30

    def __init__(self) -> None:
        self.barbs_dict = self._setup_barbs_dict("barbs.txt")
        self.player_dict = (
            self._setup_barbs_dict("players.txt")
            if os.path.exists("players.txt")
            else dict()
        )
        for k, v in self.player_dict.items():
            self.barbs_dict[k] = v

    @staticmethod
    def _setup_barbs_dict(file_name):
        barbs_dict = dict()
        with open(file_name) as f:
            for line in f.readlines():
                for coords in line.split():
                    barbs_dict[coords] = 0
        return barbs_dict

    def get_sorted_keys(self, x, y):
        return sorted(
            self.barbs_dict.keys(),
            key=lambda choords: (int(choords.split("|")[0]) - x) ** 2
            + (int(choords.split("|")[1]) - y) ** 2,
        )

    def get_arrival_time(
        self, choords, x, y, unit_sec_per_field=LC_SPEED_SEC_PER_FIELD
    ):
        timestamp = datetime.now().timestamp()
        target_x, target_y = map(int, choords.split("|"))
        distance = math.sqrt((target_x - x) ** 2 + (target_y - y) ** 2)
        unit_speed = 1 / unit_sec_per_field
        secs = distance / unit_speed
        potential_arrival = timestamp + secs
        return potential_arrival

    def get_next_targets(self, x: int, y: int) -> Iterable[Barbarian]:
        for key in self.get_sorted_keys(x, y):
            arriving_plunder = self.barbs_dict[key]

            potential_arrival = self.get_arrival_time(key, x, y)
            if arriving_plunder - potential_arrival < -self.ATTACK_INTERVAL_IN_SEC:
                yield Barbarian(choords=key)
                self.barbs_dict[key] = potential_arrival

        return None

    @staticmethod
    def get_units_based_on_points(points):
        return max(1, points // 80)


class DatabaseFarmer(SimpleFarmer):
    PRODUCTIONS_LEVELS_PER_HOUR = [
        5,
        30,
        35,
        41,
        47,
        55,
        64,
        74,
        86,
        100,
        117,
        136,
        158,
        184,
        214,
        249,
        289,
        337,
        391,
        455,
        530,
        616,
        717,
        833,
        969,
        1127,
        1311,
        1525,
        1774,
        2063,
        2400,
    ]

    def __init__(
        self, report_controller: ReportController, barb_controller: BarbController
    ) -> None:
        super().__init__()
        self.report_controller = report_controller
        self.barb_controller = barb_controller

    def get_next_targets(self, x: int, y: int):
        keys = self.get_sorted_keys(x, y)
        for key in keys:
            barb = self.barb_controller.get_barb(key)
            if barb is None:
                barb = Barbarian(id=None, choords=key)
            self.update_barb_from_report(barb)

            prod_per_sec = self.get_production_per_sec(barb)
            potential_arrival = self.get_arrival_time(key, x, y)
            if (
                barb.barb_status == BarbStatus.SCOUT_ON_THE_WAY
                or barb.barb_status == BarbStatus.RAM_ON_THE_WAY
                or barb.barb_status == BarbStatus.HOLD_OFF
            ):
                print(f"Choords: {key} has status: {barb.barb_status.name}, skipping")
                continue
            if (
                barb.barb_status == BarbStatus.NEEDS_RAMMING
                or barb.barb_status == BarbStatus.NEEDS_SCOUTING
            ):
                potential_arrival = self.get_arrival_time(
                    key,
                    x,
                    y,
                    unit_sec_per_field=30 * 60
                    if barb.barb_status.NEEDS_RAMMING
                    else 9 * 60,
                )
                barb.next_arrival = datetime.fromtimestamp(potential_arrival)
                yield barb
            elif prod_per_sec:
                available_resources = prod_per_sec * (
                    potential_arrival - barb.next_arrival.timestamp()
                )
                if (
                    available_resources
                    > self.LC_CARRY_CAPACITY * self.TARGET_EFFICIENCY
                ):
                    # FIXME ugly hack
                    self.available_resources = available_resources
                    barb.next_arrival = datetime.fromtimestamp(potential_arrival)
                    yield barb

            elif (
                barb.next_arrival.timestamp() - potential_arrival
                < -self.ATTACK_INTERVAL_IN_SEC
                or barb.barb_status == BarbStatus.NEEDS_RAMMING
            ):
                barb.next_arrival = datetime.fromtimestamp(potential_arrival)
                yield barb

    def get_production_per_sec(self, barb):
        if len(barb.buildings) == 0:
            return None
        lvls = [
            barb.buildings.get(x, 0) for x in ["Clay pit", "Timber camp", "Iron mine"]
        ]
        base_prod = sum(
            self.PRODUCTIONS_LEVELS_PER_HOUR[lvl] * self.WOLRD_SPEED for lvl in lvls
        )
        return base_prod / 3600

    def get_units(
        self, barb: Barbarian, points: int, spys_available
    ) -> Optional[Dict[str, int]]:
        point_diff = points - barb.points
        include_spies = self._should_we_include_spies(barb.buildings, points)
        if points != 0 and point_diff != 0:
            barb.points = points
            if self._is_point_diff_possibly_wall(point_diff):
                barb.barb_status = BarbStatus.NEEDS_SCOUTING
            self.barb_controller.update_barb(barb)
        if (
            self._is_point_diff_possibly_wall(point_diff)
            or barb.barb_status == BarbStatus.NEEDS_SCOUTING
        ):
            barb.barb_status = BarbStatus.SCOUT_ON_THE_WAY
            return {"spy": 1}
        elif barb.barb_status == BarbStatus.GOOD_TO_GO:
            units = {
                "light": int(
                    min(
                        self.MAXIMUM_LC_AT_A_TIME,
                        max(
                            1,
                            self.available_resources // self.LC_CARRY_CAPACITY,
                        ),
                    )
                )
                if len(barb.buildings) > 1
                else self._get_number_of_light(barb)
            }
            if (len(barb.buildings) == 0 or include_spies) and spys_available:
                units["spy"] = 1
            else:
                units["spy"] = 0
            return units
        elif barb.barb_status == BarbStatus.NEEDS_RAMMING:
            barb.barb_status = BarbStatus.RAM_ON_THE_WAY
            wall_lvl = barb.buildings["Wall"]
            return {
                "axe": 20 * wall_lvl,
                "spy": 1,
                "light": 5 * wall_lvl,
                "ram": 3 * wall_lvl,
            }

        return None

    def _should_we_include_spies(self, buildings, points):
        if points == 0:
            return False
        known_points = sum(
            map(lambda k: CUMULATIVE_POINT_TABLE[k[0]][k[1]], buildings.items())
        )
        unknown_points = abs(points - known_points)
        return unknown_points > self.UNKNOWN_POINTS_TO_TRIGGER_NEW_SPY_ATTACK

    @staticmethod
    def _is_point_diff_possibly_wall(diff):
        return diff == 8  # 8 is wall 1

    def _get_number_of_light(self, barb: Barbarian):
        reports = self.report_controller.get_last_x_reports(barb.choords, 5)
        reports = [
            r for r in reports if r.attack_force["light"] - r.losses["light"] > 0
        ]
        if not reports:
            return self.get_units_based_on_points(barb.points)
        efficiencies = [r.haul / r.capacity for r in reports]
        # median-ish efficiency
        efficiency = sorted(efficiencies)[len(efficiencies) // 2]
        mean_lcs = sum(r.attack_force["light"] for r in reports) / len(reports)
        if efficiency > 0.85:
            return int(mean_lcs) + 1
        if efficiency < 0.5:
            return max(1, int(mean_lcs) - 1)
        return int(mean_lcs)

    def last_report_okay(self, choords):
        last_report = self.report_controller.get_last_report(choords)
        return last_report is not None and sum(last_report.losses.values()) == 0

    def update_barb_from_report(self, barb):
        last_report = self.report_controller.get_last_report(barb.choords)
        # FIXME Figure out if we should not update status on barbs that has barb_status *_ON_THE_WAY
        if last_report is None:
            barb.barb_status = BarbStatus.GOOD_TO_GO
            return
        if (
            barb.barb_status == BarbStatus.NEEDS_RAMMING
            or barb.barb_status == BarbStatus.NEEDS_SCOUTING
        ):
            # we don't want to override NEEDS statuses as they may have come from point difference possibly being wall
            return
        if len(last_report.scout_report) > 0:
            barb.buildings = last_report.scout_report
        if sum(last_report.losses.values()) > 0:
            if last_report.attack_force["spy"] > 0:
                barb.barb_status = BarbStatus.HOLD_OFF
            elif barb.barb_status is not BarbStatus.SCOUT_ON_THE_WAY:
                barb.barb_status = BarbStatus.NEEDS_SCOUTING
        else:
            if last_report.attack_force["spy"] > 0:
                # FIXME add case for containing enemy troops
                if (
                    "Wall" in last_report.scout_report
                    and last_report.scout_report['Wall'] != 0
                    and barb.barb_status is not BarbStatus.RAM_ON_THE_WAY
                ):
                    barb.barb_status = BarbStatus.NEEDS_RAMMING
                elif (
                    "Wall" not in last_report.scout_report
                    or last_report.scout_report["Wall"] == 0
                ):
                    barb.barb_status = BarbStatus.GOOD_TO_GO
            elif (
                barb.barb_status != BarbStatus.SCOUT_ON_THE_WAY
                and barb.barb_status != BarbStatus.RAM_ON_THE_WAY
            ):
                barb.barb_status = BarbStatus.GOOD_TO_GO
        self.barb_controller.update_barb(barb)
