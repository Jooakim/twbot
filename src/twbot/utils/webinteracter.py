from collections import defaultdict
from typing import Tuple
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait  # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC
from twbot.model.db.barbs import BarbController
from twbot.model.defstrats import DefensiveStrategy
from twbot.model.scavenge import SimpleScavenger
from sqlalchemy.engine.create import create_engine
from sqlalchemy.orm.session import Session, sessionmaker
from twbot.model.db.report import ReportController
from twbot.model.farmers import DatabaseFarmer

from twbot.model.offstrats import EarlyOffensiveStrategy, OffensiveStrategy
from twbot.model.strategy import PassiveStrategy
from twbot.model.village import Village
from fake_useragent import UserAgent

import time
import random
import sys
import os


class WebInteracter(object):
    # TODO Move much of the village specific to the village class
    ALLOWED_BUILDING_QUEUE_SIZE = 2

    def __init__(self):
        """Start up..."""

        self._session = self._init_db()
        self._loginToTribalWars()
        self.report_controller = ReportController(
            self.driver, self.base_url, self._session
        )
        self.barb_controller = BarbController(self._session)
        self.villages = self._construct_villages()
        #self.villages = [self.village]
        #self.scav = self._config_scavenge()
        #self.optimal_scav = self.scav.generate_optimal_scavenging_spread()
        self.farmer = DatabaseFarmer(self.report_controller, self.barb_controller)

        # for index in self.optimal_scav:
        #     print(
        #         f'level {index+1}: spears:{self.optimal_scav[index]["spear"]},  swords:{self.optimal_scav[index]["sword"]}'
        #     )

        self.main_loop()

    @staticmethod
    def _init_db() -> Session:
        engine = create_engine(
            f"sqlite+pysqlite:///{os.getcwd()}/database.db", echo=False, future=True
        )
        return sessionmaker(bind=engine, autoflush=False)()

    def main_loop(self):
        tries = 10
        while True:
            try:
                self.report_controller.read_reports()
                for village in self.villages:
                    #self._upgrade_village(village)
                    unit_counts = self._get_all_units(village)
                    self._recruit_units(village, unit_counts)
                    self._farm(village, unit_counts)
                time.sleep(random.randint(30, 90))
                tries = 10
            except KeyboardInterrupt:
                print("Interrupted, quitting")
                self.driver.close()
                sys.exit(0)
            except Exception as e:
                print(f"Error: {e}")
                tries -= 1
                if tries == 0:
                    print("Too many errors, quitting")
                    self.driver.close()
                    sys.exit(0)
                # we try again
                time.sleep(random.randint(120, 300))


    # def _visit_overview(self, village):
    #     overview_url = "&screen=overview"
    #     self.driver.get(self.base_url +f'village={village.village_id}'+ overview_url)
    #     time.sleep(2)

    def _visit_headquarter(self, village):
        hq_url = "&screen=main"
        self.driver.get(self.base_url +f'village={village.village_id}'+ hq_url)
        time.sleep(2)

    def _visit_rally_point(self, village):
        rally_point_url = "&screen=place"
        self.driver.get(self.base_url + f'village={village.village_id}'+ rally_point_url)
        time.sleep(2)

    def _visit_scavange(self, village):
        scavenge_url = "&screen=place&mode=scavenge"
        self.driver.get(self.base_url + f'village={village.village_id}'+ scavenge_url)
        time.sleep(2)

    def _visit_stable(self, village):
        stable_url = "&screen=stable"
        self.driver.get(self.base_url + f'village={village.village_id}'+ stable_url)
        time.sleep(2)

    def _visit_barrack(self, village):
        barrack_url = "&screen=barracks"
        self.driver.get(self.base_url + f'village={village.village_id}'+ barrack_url)
        time.sleep(2)

    def _read_construction_levels(self, village):
        self._visit_headquarter(village)

        prefix_id = "main_buildrow_"

        buildings = dict()
        buildings["stable"] = 0
        buildings["garage"] = 0

        tmp = self.driver.find_elements_by_xpath("//*[contains(@id,'%s')]" % prefix_id)
        for l in tmp:
            # Extract the name of the construction
            # E.g main_buildrow_main, we want main
            building = l.get_attribute("id").split("_")[2]

            span = l.find_element_by_tag_name("span")
            level_string = span.text.split()[1]
            if level_string.isdigit():
                level = int(level_string)
            else:
                level = 0
            print(f"{building=} {level=}")
            buildings[building] = level

        try:
            queue_elements = self.driver.find_element_by_id(
                "buildqueue"
            ).find_elements_by_tag_name("tr")
            for element in queue_elements:
                queue_class = element.get_attribute("class")
                for class_name in queue_class.split():
                    if "buildorder" in class_name:
                        queued_building = class_name.split("_")[1]
                        buildings[queued_building] += 1
                        print(f"{queued_building=}")
        except:
            pass
        return buildings

    def _construct_villages(self):
        village_configs = {
                7215: DefensiveStrategy(),
                3366: DefensiveStrategy(),
                384: OffensiveStrategy(),
                6739: OffensiveStrategy(),
                }

        villages = []
        for village_id, strategy in village_configs.items():
            village = Village(village_id, None, strategy)
            self._visit_headquarter(village)
            village.choords = self._read_village_choords()
            villages.append(village)
        return villages

    def _read_village_choords(self) -> Tuple[int, int]:
        village_info_menu = self.driver.find_element_by_id("menu_row2")
        position_wrapper = village_info_menu.find_element_by_tag_name("b")
        choords = position_wrapper.text.split()[0][1:-1]
        return tuple(map(int, choords.split("|")))

    def _recruit_units(self, village: Village, unit_counts):
        self._recruit_stable(village.get_stable_unit(unit_counts), village)
        self._recruit_barrack(village.get_barrack_unit(unit_counts), village)

    def _recruit_stable(self, unit, village):
        if unit is None:
            print('nothing to train in stables')
            return

        self._visit_stable(village)
        try:
            queue_placeholder = self.driver.find_element_by_id("trainqueue_stable")
            n_elements = len(queue_placeholder.find_elements_by_tag_name("tr"))
            if n_elements >= 2:
                print(f"Already {n_elements} in stable queue")
                return
        except:
            print("No units queued in stable")
        try:
            lc_btn = self.driver.find_element_by_id(unit + "_0_a")
            if lc_btn.is_displayed():
                lc_btn.click()
                self.driver.find_element_by_class_name("btn-recruit").click()
        except:
            pass

    def _recruit_barrack(self, unit, village):
        if unit is None:
            print('nothing to train in barracks')
            return
        self._visit_barrack(village)
        try:
            queue_placeholder = self.driver.find_element_by_id("trainqueue_barracks")
            n_elements = len(queue_placeholder.find_elements_by_tag_name("tr"))
            if n_elements >= 2:
                print(f"Already {n_elements} in barrack queue")
                return
        except:
            print("No units queued in barrack")
        try:
            unit_btn = self.driver.find_element_by_id(unit + "_0_a")
            if unit_btn.is_displayed():
                unit_btn.click()
                self.driver.find_element_by_class_name("btn-recruit").click()
        except:
            pass

    def _can_enqueue(self):
        try:
            return (
                len(
                    self.driver.find_element_by_id(
                        "buildqueue"
                    ).find_elements_by_tag_name("tr")
                )
                <= self.ALLOWED_BUILDING_QUEUE_SIZE + 1
            )
        except:
            return True

    def _upgrade_village(self, village):
        self._visit_headquarter(village)
        if self._can_enqueue():
            try:
                buildings = self._read_construction_levels(village)
                building = village.get_building_to_update(buildings)
                building_div = self.driver.find_element_by_id(
                    "main_buildrow_" + building[0]
                )
                building_div.find_element_by_class_name("btn-build").click()
            except:
                print("could not upgrade")
        else:
            print(
                f"Currently only supports {self.ALLOWED_BUILDING_QUEUE_SIZE} in building queue"
            )

    def _farm(self, village, unit_counts):
        scav = self._config_scavenge(village, unit_counts)
        self._visit_rally_point(village)
        units = defaultdict(int)
        for unit in ["spear", "sword", "axe", "light", "spy", "ram", "knight"]:
            num_units = int(
                self.driver.find_element_by_id("units_entry_all_" + unit).text[1:-1]
            )
            units[unit] = num_units

        optimal_scav = scav.generate_optimal_scavenging_spread()
        for level in optimal_scav:
            if all(
                [
                    optimal_scav[level][a] <= units[a]
                    for a in optimal_scav[level]
                ]
            ):
                self._scavange(optimal_scav[level], level, village)
        self._plunder(units, village)

    def _get_all_units(self, village):
        self._visit_barrack(village)
        inputs = self.driver.find_element_by_id("train_form")
        units = inputs.find_elements_by_xpath("//tr[@class='row_a']/td[3]")
        unit_counts = defaultdict(int)
        for name, count in zip(["spear", "sword", "axe", "archer"], units):
            value = int(count.text.split("/")[1])
            unit_counts[name] = value

        self._visit_stable(village)
        inputs = self.driver.find_element_by_id("train_form")
        units = inputs.find_elements_by_xpath("//tr[@class='row_a']/td[3]")
        for name, count in zip(["spy", "light", "marcher", "heavy"], units):
            value = int(count.text.split("/")[1])
            unit_counts[name] = value

        print(unit_counts)
        return unit_counts

    def _config_scavenge(self, village, unit_counts):

        self._visit_scavange(village)
        scavenger_divs = self.driver.find_elements_by_class_name("scavenge-option")

        return SimpleScavenger(unit_counts, scavenger_divs)

    def _scavange(self, units, level, village):
        self._visit_scavange(village)
        WebDriverWait(self.driver, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, "unitsInput"))
        )
        inputs = self.driver.find_elements_by_class_name("unitsInput")
        for index, unit in enumerate(["spear", "sword", "axe"]):
            inputs[index].send_keys(units[unit])
            time.sleep(0.2)
        time.sleep(1)
        scavenger_divs = self.driver.find_elements_by_class_name("scavenge-option")
        try:
            # level is represented by indices in array
            button = scavenger_divs[level].find_element_by_class_name(
                "free_send_button"
            )
            button.click()
        except:
            print(f"Can't send scavenge {level=}")
            pass

    def _plunder(self, units, village):
        # Don't send too many at once
        units["light"] = min(200, units["light"])

        self._visit_rally_point(village)
        x, y = village.get_village_position()
        for barb in self.farmer.get_next_targets(x, y):
            if units["light"] <= 0:
                return
            units_after_success = units.copy()
            target_div = self.driver.find_element_by_id("place_target")
            target_input = target_div.find_element_by_class_name("target-input-field")
            attack_button = self.driver.find_element_by_id("target_attack")
            time.sleep(0.3)
            # The following sendkeys are split to avoid popup from server that says
            # we are sending too many requests
            target_input.send_keys(barb.choords[:3])
            time.sleep(0.3)
            target_input.send_keys(barb.choords[3:])
            time.sleep(0.6)
            try:

                village_info = WebDriverWait(self.driver, 2, poll_frequency=0.1).until(
                    lambda x: x.find_element_by_class_name("village-info").text
                )

                points = int(village_info.split()[-1])
                if (
                    "Owner: Barbarian" not in village_info
                    and barb.choords not in self.farmer.player_dict
                ):
                    continue
            except Exception as e:
                # print(f"Couldn't find target {barb.choords}, dont care. {e}")
                # points = 0
                print(f"Couldn't find target {barb.choords}, skipping. {e}")
                target_input.clear()
                continue
            units_to_send = self.farmer.get_units(barb, points, units["spy"] > 0)
            if units_to_send is None:
                continue

            for unit, value in units_to_send.items():
                units_after_success[unit] -= value
                unit_input = self.driver.find_element_by_id(f"unit_input_{unit}")
                unit_input.clear()
                unit_input.send_keys(value)

            time.sleep(0.2)
            success = False
            try:
                attack_button.click()
                # self.driver.find_element_by_id(
                #     "default_name_span"
                # ).find_element_by_tag_name("a").click()
                # self.driver.find_element_by_id("new_attack_name").send_keys("FARM")
                # self.driver.find_element_by_id("attack_name_btn").click()

                WebDriverWait(self.driver, 1, poll_frequency=0.1).until(
                    EC.presence_of_element_located((By.ID, "troop_confirm_go"))
                )
                self.driver.find_element_by_id("troop_confirm_go").click()
                success = True

            except:
                print(f"Couldn't attack target {barb.choords}, skipping")
                self._visit_rally_point(village)
            finally:
                if success:
                    units = units_after_success
                    # Last thing we do is update if everything went okay
                    self.barb_controller.update_barb(barb)


    def _loginToTribalWars(self):
        with open(".twlogin", "r") as f:
            self.login = f.readline().strip()
            self.password = f.readline().strip()

        url = "https://www.tribalwars.net/"

        options = webdriver.FirefoxOptions()
        ua = UserAgent()
        userAgent = ua.random
        print(userAgent)
        options.add_argument(f'user-agent={userAgent}')
        driver = webdriver.Firefox(options=options)
        # driver.install_addon(str(Path("C:/Users/magnu/Downloads/buster.xpi").absolute()))
        #driver.install_addon(str(Path('anti-captcha.xpi').absolute()))
        driver.get(url)
        user = driver.find_element_by_id("user")
        user.send_keys(self.login)
        password = driver.find_element_by_id("password")
        password.send_keys(self.password)
        password.submit()

        WebDriverWait(driver, 100).until(
            EC.presence_of_element_located((By.CLASS_NAME, "world_button_active"))
        )
        driver.find_element_by_class_name("world_button_active").click()
        try:
            driver.find_element_by_class_name("popup_box_close").click()
        except:
            pass

        self.base_url = 'https://en120.tribalwars.net/game.php?'#driver.current_url
        self.driver = driver
